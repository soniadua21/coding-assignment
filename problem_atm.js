const denominations = new Map([['2000', 3], ['500', 3], ['200', 1], ['100', 1]]);
let amountToWithdraw = 6400;
let print = (val) => console.log(val);

let dispenseMinimumNoOfNotes = (denominations, rem) => {
    if(rem % 100 !== 0 ) {
        console.log('Please provide valid amount');
        return;
    }
    let ans = {};
    denominations.forEach((count, note) => {
        if (count !== 0 && rem >= +(note)) {
            let requiredNotes = parseInt(rem / +(note));
            let availbaleNotes = count;
            const usedNotes = Math.min(requiredNotes, availbaleNotes);
            ans[note] = usedNotes
            denominations[note] -= usedNotes;
            rem -= +(note) * usedNotes;
        }
    });
    return rem > 0 ? -1 : ans;
}

const minimumNoOfNotes = dispenseMinimumNoOfNotes(denominations, amountToWithdraw);
if (minimumNoOfNotes === -1) {
    console.log("Insufficient balance");
} else {
    for (let key in minimumNoOfNotes) {
        console.log(key + ' - ' + minimumNoOfNotes[key]);
    }
}