
const colors = ['red', 'green', 'yellow', 'green', 'red', 'red', 'green', 'red', 'green', 'blue', 'yellow', 'green', 'green', 'red', 'green', 'blue']


let findMaximumColorCount = (colors) => {
    if(colors.length === 0) {
        console.log('No color found!');
        return;
    }
    let cache = {};
    let count = 0;
    let color = '';
    colors.forEach((data) => {
        cache[data] = cache.hasOwnProperty(data) ? ++cache[data] : 1
    });
    for(let key in cache) {
        if(cache.hasOwnProperty(key) && cache[key] > count) {
            count = cache[key];
            color = key;
        } 
    }
     console.log('Color '+ color + ' has come maximum no of times with count '+ count );
  }

  findMaximumColorCount(colors);